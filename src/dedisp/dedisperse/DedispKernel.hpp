#ifndef DEDISP_DEDISP_DEDISPERSE_DEDISP_KERNEL_HPP_
#define DEDISP_DEDISP_DEDISPERSE_DEDISP_KERNEL_HPP_

#include <string>

#include <cudawrappers/cu.hpp>

#include "common/dedisp_types.h"
#include <GPUKernel.hpp>

extern const char _binary_src_dedisp_dedisperse_dedisperse_kernel_cu_start,
    _binary_src_dedisp_dedisperse_dedisperse_kernel_cu_end;

class DedisperseKernel : public GPUKernel {
public:
  DedisperseKernel()
      : GPUKernel(
            "dedisperse_kernel.cu", "dedisperse_kernel",
            std::string(
                reinterpret_cast<const char *>(
                    &_binary_src_dedisp_dedisperse_dedisperse_kernel_cu_start),
                reinterpret_cast<const char *>(
                    &_binary_src_dedisp_dedisperse_dedisperse_kernel_cu_end))) {
  }

  bool run(const dedisp_word *d_in, dedisp_size in_stride, dedisp_size nsamps,
           dedisp_size in_nbits, dedisp_size nchans, dedisp_size chan_stride,
           const dedisp_float *d_dm_list, dedisp_size dm_count,
           dedisp_size dm_stride, dedisp_byte *d_out, dedisp_size out_stride,
           dedisp_size out_nbits, dedisp_size batch_size,
           dedisp_size batch_in_stride, dedisp_size batch_dm_stride,
           dedisp_size batch_chan_stride, dedisp_size batch_out_stride,
           const void *delay_table, const size_t delay_table_size,
           const void *killmask, const size_t killmask_size,
           cu::Stream &h2dstream, cu::Stream &stream);

private:
  const void *delay_table_src_host_ = nullptr;
  size_t delay_table_count_ = 0;
  size_t delay_table_offset_ = 0;

  const void *killmask_src_host_ = nullptr;
  size_t killmask_count_ = 0;
  size_t killmask_offset_ = 0;
};

#endif // DEDISP_DEDISP_DEDISPERSE_DEDISP_KERNEL_HPP_