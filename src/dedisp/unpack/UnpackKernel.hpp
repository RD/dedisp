#ifndef DEDISP_DEDISP_DEDISPERSE_UNPACK_KERNEL_HPP_
#define DEDISP_DEDISP_DEDISPERSE_UNPACK_KERNEL_HPP_

#include <cudawrappers/cu.hpp>
#include <string>

#include "common/dedisp_types.h"
#include <GPUKernel.hpp>

class UnpackKernel {
public:
  void run(const dedisp_word *d_transposed, dedisp_size nsamps,
           dedisp_size nchan_words, dedisp_word *d_unpacked,
           dedisp_size in_nbits, dedisp_size out_nbits, cu::Stream &);
};

#endif // DEDISP_DEDISP_DEDISPERSE_UNPACK_KERNEL_HPP_