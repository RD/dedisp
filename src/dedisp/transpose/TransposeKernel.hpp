#ifndef DEDISP_DEDISP_DEDISPERSE_TRANSPOSE_KERNEL_H_
#define DEDISP_DEDISP_DEDISPERSE_TRANSPOSE_KERNEL_H_

#include <cudawrappers/cu.hpp>

#include "common/dedisp_types.h"
#include <GPUKernel.hpp>

extern const char _binary_src_dedisp_transpose_transpose_kernel_cu_start,
    _binary_src_dedisp_transpose_transpose_kernel_cu_end;

class TransposeKernel : public GPUKernel {
public:
  TransposeKernel()
      : GPUKernel(
            "transpose_kernel.cu", "transpose_kernel_grid",
            std::string(
                reinterpret_cast<const char *>(
                    &_binary_src_dedisp_transpose_transpose_kernel_cu_start),
                reinterpret_cast<const char *>(
                    &_binary_src_dedisp_transpose_transpose_kernel_cu_end))) {}

  void run(cu::DeviceMemory &in, size_t width, size_t height, size_t in_stride,
           size_t out_stride, cu::DeviceMemory &out, cu::Stream &stream);
};

#endif // DEDISP_DEDISP_DEDISPERSE_TRANSPOSE_KERNEL_H_