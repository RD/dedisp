#include "DedispKernel.hpp"
#include "GPUKernel.hpp"
#include "dedisperse_kernel.cu"

#include <cudawrappers/cu.hpp>
#include <iostream>
#include <memory>

/*
 * Helper functions
 */

/*
 * dedisperse routine
 */
bool DedisperseKernel::run(
    const dedisp_word *d_in, dedisp_size in_stride, dedisp_size nsamps,
    dedisp_size in_nbits, dedisp_size nchans, dedisp_size chan_stride,
    const dedisp_float *d_dm_list, dedisp_size dm_count, dedisp_size dm_stride,
    dedisp_byte *d_out, dedisp_size out_stride, dedisp_size out_nbits,
    dedisp_size batch_size, dedisp_size batch_in_stride,
    dedisp_size batch_dm_stride, dedisp_size batch_chan_stride,
    dedisp_size batch_out_stride, const void *delay_table,
    const size_t delay_table_size, const void *killmask,
    const size_t killmask_size, cu::Stream &htodstream,
    cu::Stream &executestream) {
  // Define thread decomposition
  // Note: Block dimensions x and y represent time samples and DMs respectively
  dim3 block(BLOCK_DIM_X, BLOCK_DIM_Y);
  // Note: Grid dimension x represents time samples. Dimension y represents
  //         DMs and batch jobs flattened together.

  // Divide and round up
  dedisp_size nsamp_blocks =
      (nsamps - 1) / ((dedisp_size)DEDISP_SAMPS_PER_THREAD * block.x) + 1;
  dedisp_size ndm_blocks = (dm_count - 1) / (dedisp_size)block.y + 1;

  // Constrain the grid size to the maximum allowed
  // TODO: Consider cropping the batch size dimension instead and looping over
  // it inside the kernel
  ndm_blocks = min((unsigned int)ndm_blocks,
                   (unsigned int)(MAX_CUDA_GRID_SIZE_Y / batch_size));

  // Note: We combine the DM and batch dimensions into one
  dim3 grid(nsamp_blocks, ndm_blocks * batch_size);

  // Divide and round up
  dedisp_size nsamps_reduced = (nsamps - 1) / DEDISP_SAMPS_PER_THREAD + 1;

  const std::vector<const void *> parameters = {&d_in,
                                                &nsamps,
                                                &nsamps_reduced,
                                                &nsamp_blocks,
                                                &in_stride,
                                                &dm_count,
                                                &dm_stride,
                                                &ndm_blocks,
                                                &nchans,
                                                &chan_stride,
                                                &d_out,
                                                &out_nbits,
                                                &out_stride,
                                                &d_dm_list,
                                                &batch_in_stride,
                                                &batch_dm_stride,
                                                &batch_chan_stride,
                                                &batch_out_stride};

  std::ostringstream ss;
  ss << "-DPARAM_IN_NBITS=" << in_nbits;

  CompiledKernelInfo kernel;
  bool did_recompile;

  std::tie(kernel, did_recompile) = compile({ss.str()});
  assertCompiled(kernel);

  // Copy delay table and killmask
  if (did_recompile) {
    if (delay_table) {
      htodstream.memcpyHtoDAsync(kernel.module->getGlobal("c_delay_table"),
                                 delay_table, delay_table_size);
      htodstream.synchronize();
    }

    if (killmask) {
      htodstream.memcpyHtoDAsync(kernel.module->getGlobal("c_killmask"),
                                 killmask, killmask_size);
      htodstream.synchronize();
    }
  }

  executestream.launchKernel(*(kernel.function), grid.x, grid.y, grid.z,
                             block.x, block.y, block.z, 0, parameters);

  return true;
}
