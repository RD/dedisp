#include "dedisp.h"

#include "fdd/FDDGPUPlan.hpp"

#include <cmath>
#include <memory>

#include <cudawrappers/cu.hpp>

#ifdef __cplusplus
extern "C" {
#endif

struct dedisp_plan_struct {
  std::shared_ptr<dedisp::Plan> plan_ = nullptr;
};

// Global device index
static int device_idx_ = 0;

// Internal abstraction for errors
#ifdef DEDISP_DEBUG
#define throw_error(error)                                                     \
  do {                                                                         \
    printf("An error occurred within dedisp on line %d of %s: %s", __LINE__,   \
           __FILE__, dedisp_get_error_string(error));                          \
    return (error);                                                            \
  } while (0)
#define throw_getter_error(error, retval)                                      \
  do {                                                                         \
    printf("An error occurred within dedisp on line %d of %s: %s", __LINE__,   \
           __FILE__, dedisp_get_error_string(error));                          \
    return (retval);                                                           \
  } while (0)
#else
#define throw_error(error) return error
#define throw_getter_error(error, retval) return retval
#endif // DEDISP_DEBUG

dedisp_error dedisp_create_plan(dedisp_plan *plan, dedisp_size nchans,
                                dedisp_float dt, dedisp_float f0,
                                dedisp_float df) {
#ifdef DEDISP_DEBUG
  std::cout << "dedisp_create_plan()" << std::endl;
#endif

  *plan = nullptr;

  *plan = new dedisp_plan_struct();
  if (!plan) {
    throw_error(DEDISP_MEM_ALLOC_FAILED);
  }

  try {
    (*plan)->plan_.reset(
        new dedisp::FDDGPUPlan(nchans, dt, f0, df, device_idx_));
  } catch (...) {
    dedisp_destroy_plan(*plan);
    throw_error(DEDISP_UNKNOWN_ERROR);
  }

  return DEDISP_NO_ERROR;
}

dedisp_error dedisp_set_gulp_size(dedisp_plan plan, dedisp_size gulp_size) {
  if (!plan) {
    throw_error(DEDISP_INVALID_PLAN);
  }

  return DEDISP_NOT_IMPLEMENTED_ERROR;
}

dedisp_size dedisp_get_gulp_size(dedisp_plan plan) {
  if (!plan) {
    throw_error(DEDISP_INVALID_PLAN);
  }

  throw_error(DEDISP_NOT_IMPLEMENTED_ERROR);

  return 0;
}

dedisp_error dedisp_set_dm_list(dedisp_plan plan, const dedisp_float *dm_list,
                                dedisp_size count) {
  if (!plan) {
    throw_error(DEDISP_INVALID_PLAN);
  }

  try {
    plan->plan_->set_dm_list(dm_list, count);
  } catch (...) {
    throw_error(DEDISP_UNKNOWN_ERROR);
  }

  return DEDISP_NO_ERROR;
}

dedisp_error dedisp_generate_dm_list(dedisp_plan plan, dedisp_float dm_start,
                                     dedisp_float dm_end, dedisp_float ti,
                                     dedisp_float tol) {
  if (!plan) {
    throw_error(DEDISP_INVALID_PLAN);
  }

  try {
    plan->plan_->generate_dm_list(dm_start, dm_end, ti, tol);
  } catch (...) {
    throw_error(DEDISP_UNKNOWN_ERROR);
  }

  return DEDISP_NO_ERROR;
}

void generate_dm_list(std::vector<dedisp_float> &dm_table,
                      dedisp_float dm_start, dedisp_float dm_end, double dt,
                      double ti, double f0, double df, dedisp_size nchans,
                      double tol) {
  // Note: This algorithm originates from Lina Levin
  // Note: Computation done in double precision to match MB's code

  dt *= 1e6;
  double f = (f0 + ((nchans / 2) - 0.5) * df) * 1e-3;
  double tol2 = tol * tol;
  double a = 8.3 * df / (f * f * f);
  double a2 = a * a;
  double b2 = a2 * (double)(nchans * nchans / 16.0);
  double c = (dt * dt + ti * ti) * (tol2 - 1.0);

  dm_table.push_back(dm_start);
  while (dm_table.back() < dm_end) {
    double prev = dm_table.back();
    double prev2 = prev * prev;
    double k = c + tol2 * a2 * prev2;
    double dm =
        ((b2 * prev + std::sqrt(-a2 * b2 * prev2 + (a2 + b2) * k)) / (a2 + b2));
    dm_table.push_back(dm);
  }
}

dedisp_float *dedisp_generate_dm_list_guru(dedisp_float dm_start,
                                           dedisp_float dm_end, double dt,
                                           double ti, double f0, double df,
                                           dedisp_size nchans, double tol,
                                           dedisp_size *dm_count) {
  std::vector<dedisp_float> dm_table;
  generate_dm_list(dm_table, dm_start, dm_end, dt, ti, f0, df, nchans, tol);
  *dm_count = dm_table.size();
  return &dm_table[0];
}

dedisp_error dedisp_set_device(int device_idx) {
  // keep global copy of device_idx for usage in dedisp_create_plan()
  device_idx_ = device_idx;

  return DEDISP_NO_ERROR;
}

dedisp_error dedisp_set_killmask(dedisp_plan plan,
                                 const dedisp_bool *killmask) {
  if (!plan) {
    throw_error(DEDISP_INVALID_PLAN);
  }

  try {
    plan->plan_->set_killmask(killmask);
  } catch (...) {
    throw_error(DEDISP_UNKNOWN_ERROR);
  }

  return DEDISP_NO_ERROR;
}

// Getters
// -------
dedisp_size dedisp_get_max_delay(const dedisp_plan plan) {
  if (!plan) {
    throw_getter_error(DEDISP_INVALID_PLAN, 0);
  }
  if (0 == plan->plan_->get_dm_count()) {
    throw_getter_error(DEDISP_NO_DM_LIST_SET, 0);
  }
  return plan->plan_->get_max_delay();
}

dedisp_size dedisp_get_dm_delay(const dedisp_plan plan, int dm_trial) {
  if (!plan) {
    throw_getter_error(DEDISP_INVALID_PLAN, 0);
  }
  if (0 == plan->plan_->get_dm_count()) {
    throw_getter_error(DEDISP_NO_DM_LIST_SET, 0);
  }
  if (dm_trial < 0 || (dedisp_size)dm_trial >= plan->plan_->get_dm_count()) {
    throw_getter_error(DEDISP_UNKNOWN_ERROR, 0);
  }
  return (
      plan->plan_->get_dm_list()[dm_trial] *
          plan->plan_->get_delay_table()[plan->plan_->get_channel_count() - 1] +
      0.5);
}

dedisp_size dedisp_get_channel_count(const dedisp_plan plan) {
  if (!plan) {
    throw_getter_error(DEDISP_INVALID_PLAN, 0);
  }
  return plan->plan_->get_channel_count();
}

dedisp_size dedisp_get_dm_count(const dedisp_plan plan) {
  if (!plan) {
    throw_getter_error(DEDISP_INVALID_PLAN, 0);
  }
  return plan->plan_->get_dm_count();
}

const dedisp_float *dedisp_get_dm_list(const dedisp_plan plan) {
  if (!plan) {
    throw_getter_error(DEDISP_INVALID_PLAN, 0);
  }
  if (0 == plan->plan_->get_dm_count()) {
    throw_getter_error(DEDISP_NO_DM_LIST_SET, 0);
  }
  return &plan->plan_->get_dm_list()[0];
}

const dedisp_bool *dedisp_get_killmask(const dedisp_plan plan) {
  if (!plan) {
    throw_getter_error(DEDISP_INVALID_PLAN, 0);
  }
  return &plan->plan_->get_killmask()[0];
}

dedisp_float dedisp_get_dt(const dedisp_plan plan) {
  if (!plan) {
    throw_getter_error(DEDISP_INVALID_PLAN, 0);
  }
  return plan->plan_->get_dt();
}

dedisp_float dedisp_get_f0(const dedisp_plan plan) {
  if (!plan) {
    throw_getter_error(DEDISP_INVALID_PLAN, 0);
  }
  return plan->plan_->get_f0();
}

dedisp_float dedisp_get_df(const dedisp_plan plan) {
  if (!plan) {
    throw_getter_error(DEDISP_INVALID_PLAN, 0);
  }
  return plan->plan_->get_df();
}

// Plan execution
// --------------
dedisp_error dedisp_execute_guru(const dedisp_plan plan, dedisp_size nsamps,
                                 const dedisp_byte *in, dedisp_size in_nbits,
                                 dedisp_size in_stride, dedisp_byte *out,
                                 dedisp_size out_nbits, dedisp_size out_stride,
                                 dedisp_size first_dm_idx, dedisp_size dm_count,
                                 unsigned flags) {
  if (!plan) {
    throw_error(DEDISP_INVALID_PLAN);
  }

  if (flags) {
    throw_error(DEDISP_INVALID_FLAG_COMBINATION);
  }

  return DEDISP_NOT_IMPLEMENTED_ERROR;
}

dedisp_error dedisp_execute_adv(const dedisp_plan plan, dedisp_size nsamps,
                                const dedisp_byte *in, dedisp_size in_nbits,
                                dedisp_size in_stride, dedisp_byte *out,
                                dedisp_size out_nbits, dedisp_size out_stride,
                                unsigned flags) {
  if (!plan) {
    throw_error(DEDISP_INVALID_PLAN);
  }

  if (flags) {
    throw_error(DEDISP_INVALID_FLAG_COMBINATION);
  }

  return DEDISP_NOT_IMPLEMENTED_ERROR;
}

// TODO: Consider having the user specify nsamps_computed instead of nsamps
dedisp_error dedisp_execute(const dedisp_plan plan, dedisp_size nsamps,
                            const dedisp_byte *in, dedisp_size in_nbits,
                            dedisp_byte *out, dedisp_size out_nbits,
                            unsigned flags) {
  if (!plan) {
    throw_error(DEDISP_INVALID_PLAN);
  }

  try {
    plan->plan_->execute(nsamps, in, in_nbits, out, out_nbits, flags);
  } catch (...) {
    throw_error(DEDISP_UNKNOWN_ERROR);
  }

  return DEDISP_NO_ERROR;
}

dedisp_error dedisp_sync(void) {
  try {
    cu::Context::synchronize();
  } catch (...) {
    throw_error(DEDISP_PRIOR_GPU_ERROR);
  }

  return DEDISP_NO_ERROR;
}

void dedisp_destroy_plan(dedisp_plan plan) {
  if (plan) {
    delete plan;
  }
}

const char *dedisp_get_error_string(dedisp_error error) {
  switch (error) {
  case DEDISP_NO_ERROR:
    return "No error";
  case DEDISP_MEM_ALLOC_FAILED:
    return "Memory allocation failed";
  case DEDISP_MEM_COPY_FAILED:
    return "Memory copy failed";
  case DEDISP_INVALID_DEVICE_INDEX:
    return "Invalid device index";
  case DEDISP_DEVICE_ALREADY_SET:
    return "Device is already set and cannot be changed";
  case DEDISP_NCHANS_EXCEEDS_LIMIT:
    return "No. channels exceeds internal limit";
  case DEDISP_INVALID_PLAN:
    return "Invalid plan";
  case DEDISP_INVALID_POINTER:
    return "Invalid pointer";
  case DEDISP_INVALID_STRIDE:
    return "Invalid stride";
  case DEDISP_NO_DM_LIST_SET:
    return "No DM list has been set";
  case DEDISP_TOO_FEW_NSAMPS:
    return "No. samples < maximum delay";
  case DEDISP_INVALID_FLAG_COMBINATION:
    return "Invalid flag combination";
  case DEDISP_UNSUPPORTED_IN_NBITS:
    return "Unsupported in_nbits value";
  case DEDISP_UNSUPPORTED_OUT_NBITS:
    return "Unsupported out_nbits value";
  case DEDISP_PRIOR_GPU_ERROR:
    return "Prior GPU error.";
  case DEDISP_INTERNAL_GPU_ERROR:
    return "Internal GPU error. Please contact the author(s).";
  case DEDISP_NOT_IMPLEMENTED_ERROR:
    return "Functionality not implemented.";
  case DEDISP_UNKNOWN_ERROR:
    return "Unknown error. Please contact the author(s).";
  default:
    return "Invalid error code";
  }
}

dedisp_error dedisp_enable_adaptive_dt(dedisp_plan plan,
                                       dedisp_float pulse_width,
                                       dedisp_float tol) {
  return DEDISP_NOT_IMPLEMENTED_ERROR;
}

dedisp_error dedisp_disable_adaptive_dt(dedisp_plan plan) {
  return DEDISP_NOT_IMPLEMENTED_ERROR;
}

dedisp_bool dedisp_using_adaptive_dt(const dedisp_plan plan) { return false; }

const dedisp_size *dedisp_get_dt_factors(const dedisp_plan plan) {
  return plan->plan_->get_dt_factors();
}

#ifdef __cplusplus
}
#endif
