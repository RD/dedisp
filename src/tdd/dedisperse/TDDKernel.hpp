#ifndef DEDISP_TDD_DEDISPERSE_TDD_KERNEL_HPP_
#define DEDISP_TDD_DEDISPERSE_TDD_KERNEL_HPP_

#include <cudawrappers/cu.hpp>
#include <cudawrappers/nvtx.hpp>

#include <GPUKernel.hpp>

#include "common/dedisp_types.h"

extern unsigned char _binary_src_tdd_dedisperse_tdd_dedisperse_kernel_cu_start,
    _binary_src_tdd_dedisperse_tdd_dedisperse_kernel_cu_end;

class DedispKernel : public GPUKernel {
public:
  DedispKernel()
      : GPUKernel(
            "tdd_dedisperse_kernel.cu", "dedisperse_kernel",
            std::string(
                reinterpret_cast<const char *>(
                    &_binary_src_tdd_dedisperse_tdd_dedisperse_kernel_cu_start),
                reinterpret_cast<const char *>(
                    &_binary_src_tdd_dedisperse_tdd_dedisperse_kernel_cu_end))) {
  }

  unsigned int get_nsamps_per_thread();

  void run(const cu::DeviceMemory &d_in, dedisp_size in_stride,
           dedisp_size nsamps, dedisp_size in_nbits, dedisp_size nchans,
           dedisp_size chan_stride, const cu::DeviceMemory &d_dm_list,
           dedisp_size dm_count, dedisp_size dm_stride,
           const cu::DeviceMemory &d_out, dedisp_size out_stride,
           dedisp_size out_nbits, const void *delay_table,
           dedisp_size delay_table_size, const void *killmask,
           dedisp_size killmask_size, cu::Stream &htodstream,
           cu::Stream &executestream);

private:
  dedisp_word *m_d_in = nullptr;
  nvtx::Marker constant_memcpy_marker{"copy_constant_memory",
                                      nvtx::Marker::yellow};
};

#endif // DEDISP_TDD_DEDISPERSE_TDD_KERNEL_HPP_