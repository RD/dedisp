#ifndef DEDISP_FDD_UNPACK_UNPACK_KERNEL_HPP_
#define DEDISP_FDD_UNPACK_UNPACK_KERNEL_HPP_

#include "cudawrappers/cu.hpp"
#include <GPUKernel.hpp>

extern const char _binary_src_fdd_unpack_transpose_unpack_kernel_cu_start,
    _binary_src_fdd_unpack_transpose_unpack_kernel_cu_end;

class FDDKernelUnpack : public GPUKernel {
public:
  FDDKernelUnpack()
      : GPUKernel(
            "transpose_unpack_kernel.cu", "transpose_unpack_kernel_uint",
            std::string(
                reinterpret_cast<const char *>(
                    &_binary_src_fdd_unpack_transpose_unpack_kernel_cu_start),
                reinterpret_cast<const char *>(
                    &_binary_src_fdd_unpack_transpose_unpack_kernel_cu_end))) {}

  void run(const cu::DeviceMemory &d_in, size_t width, size_t height,
           size_t in_stride, size_t out_stride, cu::DeviceMemory &d_out,
           unsigned long in_nbits, unsigned long out_nbits, float scale,
           cu::Stream &stream);
};

#endif // DEDISP_FDD_UNPACK_UNPACK_KERNEL_HPP_