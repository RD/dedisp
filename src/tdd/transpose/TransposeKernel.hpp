#ifndef DEDISP_TDD_TRANSPOSE_TRANSPOSE_KERNEL_HPP_
#define DEDISP_TDD_TRANSPOSE_TRANSPOSE_KERNEL_HPP_

#include <cudawrappers/cu.hpp>

#include "common/dedisp_types.h"
#include <GPUKernel.hpp>

extern const char _binary_src_tdd_transpose_tdd_transpose_kernel_cu_start,
    _binary_src_tdd_transpose_tdd_transpose_kernel_cu_end;

class TransposeKernelTDD : public GPUKernel {
public:
  TransposeKernelTDD()
      : GPUKernel(
            "tdd_transpose_kernel.cu", "transpose_kernel_dedisp",
            std::string(
                reinterpret_cast<const char *>(
                    &_binary_src_tdd_transpose_tdd_transpose_kernel_cu_start),
                reinterpret_cast<const char *>(
                    &_binary_src_tdd_transpose_tdd_transpose_kernel_cu_end))) {}

  void run(cu::DeviceMemory &d_in, size_t width, size_t height,
           size_t in_stride, size_t out_stride, cu::DeviceMemory &d_out,
           cu::Stream &stream);
};

#endif // DEDISP_TDD_TRANSPOSE_TRANSPOSE_KERNEL_HPP_