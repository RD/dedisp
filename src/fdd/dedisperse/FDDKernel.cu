#include "FDDKernel.hpp"

#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <vector>

#include "GPUKernel.hpp"
#include "fdd_dedisperse_kernel.cu"
#include <cudawrappers/cu.hpp>

/*
 * dedisperse routine
 */
void FDDKernelDedisperse::run(
    unsigned long ndm, unsigned long nfreq, unsigned long nchan, float dt,
    const cu::DeviceMemory &d_spin_frequencies,
    const cu::DeviceMemory &d_dm_list, const cu::DeviceMemory &d_in,
    const cu::DeviceMemory &d_out, unsigned long in_stride,
    unsigned long out_stride, unsigned int idm_start, unsigned int idm_end,
    unsigned int ichan_start, const void *delay_table,
    const size_t delay_table_count, cu::Stream &htodstream,
    cu::Stream &executestream) {
  // Define thread decomposition
  unsigned grid_x = std::max((int)((ndm + NDM_BATCH_GRID) / NDM_BATCH_GRID), 1);
  unsigned grid_y = NFREQ_BATCH_GRID;
  dim3 grid(grid_x, grid_y);
  dim3 block(NFREQ_BATCH_BLOCK);

  /* Execute the kernel
   *  The second kernel argument can be set to true
   *  in order to enable an experimental optimization feature,
   *  where extrapolation is used in the computation of the phasors.
   *  Boudary conditions should be further explored to determine
   *  functional correctness at all times.
   *  Leaving this feature in because it might be beneficial
   *  depending on the system configurations.
   */

  const std::vector<const void *> parameters = {
      &nfreq, &dt,    &d_spin_frequencies, &d_dm_list, &in_stride,  &out_stride,
      &d_in,  &d_out, &idm_start,          &idm_end,   &ichan_start};

  // Execute the kernel
  std::ostringstream oss;
  oss << "-DDEF_NCHAN=" << nchan;

  CompiledKernelInfo kernel;
  bool did_recompile = false;

  std::tie(kernel, did_recompile) =
      compile({"-DDEF_EXTRAPOLATE=false", oss.str()});
  assertCompiled(kernel);

  if (did_recompile) {
    htodstream.memcpyHtoDAsync(kernel.module->getGlobal("c_delay_table"),
                               delay_table, delay_table_count);
  }

  executestream.launchKernel(*(kernel.function), grid.x, grid.y, grid.z,
                             block.x, block.y, block.z, 0, parameters);
}

/*
 * dedisperse routine
 */
void FDDKernelScale::scale(dedisp_size height, dedisp_size width,
                           dedisp_size stride, dedisp_float scale,
                           const cu::DeviceMemory &d_data, cu::Stream &stream) {

  CompiledKernelInfo kernel;
  bool did_recompile;

  std::tie(kernel, did_recompile) = compile();
  assertCompiled(kernel);

  // Define thread decomposition
  dim3 grid(height);
  dim3 block(128);

  // Execute the kernel
  std::vector<const void *> parameters = {&width, &stride, &scale, &d_data};

  stream.launchKernel(*(kernel.function), grid.x, grid.y, grid.z, block.x,
                      block.y, block.z, 0, parameters);
}
