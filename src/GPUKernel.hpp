#ifndef DEDISP_GPU_RUNTIME_KERNEL_HPP_
#define DEDISP_GPU_RUNTIME_KERNEL_HPP_

#include <memory>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <unordered_map>
#include <vector>

#include <cudawrappers/cu.hpp>
#include <cudawrappers/nvrtc.hpp>

struct CompiledKernelInfo {
  std::shared_ptr<cu::Module> module;
  std::shared_ptr<cu::Function> function;
};

class GPUKernel {
public:
  GPUKernel(const std::string &filename, const std::string &func_name,
            const std::string &file_src)
      : filename_(filename), func_name_(func_name),
        program_(std::make_unique<nvrtc::Program>(file_src, filename)) {}
  void setFunctionParams(const std::string &kernel_function_name,
                         const std::vector<const void *> &params);
  void assertCompiled(const CompiledKernelInfo &kernel_info) const;

  inline std::string getFilename() const { return filename_; }

protected:
  std::pair<const CompiledKernelInfo &, bool>
  compile(const std::vector<std::string> &compile_options = {});

private:
  const std::string filename_;
  const std::string func_name_;
  const std::unique_ptr<nvrtc::Program> program_;

  std::map<std::string, CompiledKernelInfo> compiled_kernels_;
};
#endif // DEDISP_GPU_RUNTIME_KERNEL_HPP_