#ifndef DEDISP_DEDISP_DEDISPERSE_CONSTANTS_CUH_
#define DEDISP_DEDISP_DEDISPERSE_CONSTANTS_CUH_

#define DEDISP_DEFAULT_GULP_SIZE 65536u // 131072

// TODO: Make sure this doesn't limit GPU constant memory
//         available to users.
#define DEDISP_MAX_NCHANS 8192u

// Kernel tuning parameters
#define DEDISP_BLOCK_SIZE 256u
#define DEDISP_BLOCK_SAMPS 8u
#define DEDISP_SAMPS_PER_THREAD 2u // 4 is better for Fermi?

#define BITS_PER_BYTE 8
#define BYTES_PER_WORD (sizeof(dedisp_word) / sizeof(unsigned char))
#define MAX_CUDA_GRID_SIZE_X 65535u
#define MAX_CUDA_GRID_SIZE_Y 65535u

constexpr unsigned int BLOCK_DIM_X = DEDISP_BLOCK_SAMPS;
constexpr unsigned int BLOCK_DIM_Y = (DEDISP_BLOCK_SIZE / DEDISP_BLOCK_SAMPS);

#endif // DEDISP_DEDISP_DEDISPERSE_CONSTANTS_CUH_