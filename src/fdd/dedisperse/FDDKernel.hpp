#ifndef FDD_KERNEL_H_
#define FDD_KERNEL_H_

#include <cudawrappers/cu.hpp>
#include <cudawrappers/nvtx.hpp>

#include "common/dedisp_types.h"
#include <GPUKernel.hpp>

extern const char _binary_src_fdd_dedisperse_fdd_dedisperse_kernel_cu_start,
    _binary_src_fdd_dedisperse_fdd_dedisperse_kernel_cu_end;

extern const char _binary_src_fdd_dedisperse_fdd_scale_kernel_cu_start,
    _binary_src_fdd_dedisperse_fdd_scale_kernel_cu_end;

class FDDKernelDedisperse : public GPUKernel {
public:
  FDDKernelDedisperse()
      : GPUKernel(
            "fdd_dedisperse_kernel.cu", "dedisperse_kernel_runtime",
            std::string(
                reinterpret_cast<const char *>(
                    &_binary_src_fdd_dedisperse_fdd_dedisperse_kernel_cu_start),
                reinterpret_cast<const char *>(
                    &_binary_src_fdd_dedisperse_fdd_dedisperse_kernel_cu_end))) {
  }

  void run(dedisp_size ndm, dedisp_size nfreq, dedisp_size nchan, float dt,
           const cu::DeviceMemory &d_spin_frequencies,
           const cu::DeviceMemory &d_dm_list, const cu::DeviceMemory &d_in,
           const cu::DeviceMemory &d_out, dedisp_size in_stride,
           dedisp_size out_stride, unsigned int idm_start, unsigned int idm_end,
           unsigned int ichan_start, const void *delay_table,
           const size_t delay_table_count, cu::Stream &htodstream,
           cu::Stream &executestream);
};

class FDDKernelScale : public GPUKernel {
public:
  FDDKernelScale()
      : GPUKernel(
            "fdd_kernel_scale.cu", "scale_output_kernel",
            std::string(
                reinterpret_cast<const char *>(
                    &_binary_src_fdd_dedisperse_fdd_scale_kernel_cu_start),
                reinterpret_cast<const char *>(
                    &_binary_src_fdd_dedisperse_fdd_scale_kernel_cu_end))) {}

  void scale(dedisp_size height, dedisp_size width, dedisp_size stride,
             dedisp_float scale, const cu::DeviceMemory &d_data,
             cu::Stream &stream);
};

#endif // FDD_KERNEL_H_