/*
 * Time Domain Dedispersion (TDD)
 * is an optimized version of the original dedisp implementation.
 */
#include "GPUKernel.hpp"
#include "TDDKernel.hpp"
#include "dedisp_types.h"
#include "tdd_dedisperse_kernel.cu"

#include <cudawrappers/cu.hpp>

#define DEDISP_BLOCK_SIZE 256
#define DEDISP_BLOCK_SAMPS 8

unsigned int DedispKernel::get_nsamps_per_thread() {
  return DEDISP_SAMPS_PER_THREAD;
}

/*
 * dedisperse routine
 */
void DedispKernel::run(const cu::DeviceMemory &d_in, dedisp_size in_stride,
                       dedisp_size nsamps, dedisp_size in_nbits,
                       dedisp_size nchans, dedisp_size chan_stride,
                       const cu::DeviceMemory &d_dm_list, dedisp_size dm_count,
                       dedisp_size dm_stride, const cu::DeviceMemory &d_out,
                       dedisp_size out_stride, dedisp_size out_nbits,
                       const void *delay_table, dedisp_size delay_table_size,
                       const void *killmask, dedisp_size killmask_size,
                       cu::Stream &htodstream, cu::Stream &executestream) {

  enum {
    BITS_PER_BYTE = 8,
  };

  // Define thread decomposition
  // Note: Block dimensions x and y represent time samples and DMs respectively
  dim3 block(KERNEL_BLOCK_DIM_X, KERNEL_BLOCK_DIM_Y);
  // Note: Grid dimension x represents time samples. Dimension y represents DMs

  // Divide and round up
  dedisp_size nsamp_blocks =
      (nsamps - 1) / ((dedisp_size)DEDISP_SAMPS_PER_THREAD * block.x) + 1;
  dedisp_size ndm_blocks = (dm_count - 1) / (dedisp_size)block.y + 1;

  // Constrain the grid size to the maximum allowed
  ndm_blocks =
      min((unsigned int)ndm_blocks, (unsigned int)(MAX_CUDA_GRID_SIZE_Y));

  dim3 grid(nsamp_blocks, ndm_blocks);

  // Divide and round up
  dedisp_size nsamps_reduced = (nsamps - 1) / DEDISP_SAMPS_PER_THREAD + 1;

  const std::vector<const void *> parameters = {
      &d_in,     &nsamps,    &nsamps_reduced, &nsamp_blocks, &in_stride,
      &dm_count, &dm_stride, &ndm_blocks,     &nchans,       &chan_stride,
      &d_out,    &out_nbits, &out_stride,     &d_dm_list};

  // Execute the kernel
  std::ostringstream ss;
  ss << "-DPARAM_IN_NBITS=" << in_nbits;

  CompiledKernelInfo kernel;
  bool did_recompile;
  std::tie(kernel, did_recompile) = compile({ss.str()});
  assertCompiled(kernel);

  if (did_recompile) {
    constant_memcpy_marker.start();
    if (delay_table) {
      htodstream.memcpyHtoDAsync(kernel.module->getGlobal("c_delay_table"),
                                 delay_table, delay_table_size);
      htodstream.synchronize();
    }

    if (killmask) {
      htodstream.memcpyHtoDAsync(kernel.module->getGlobal("c_killmask"),
                                 killmask, killmask_size);
      htodstream.synchronize();
    }
    constant_memcpy_marker.end();
  }

  executestream.launchKernel(*kernel.function, grid.x, grid.y, grid.z, block.x,
                             block.y, block.z, 0, parameters);
}