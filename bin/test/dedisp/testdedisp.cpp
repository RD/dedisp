#include "dedisp/DedispPlan.hpp"

#include "../common/test.hpp"

template <typename PlanType> int run(const TestInput &, TestOutput &);

int main(int argc, char *argv[]) {
  TestInput test_input{};
  TestOutput test_output{};

  return run<dedisp::DedispPlan>(test_input,
                                 test_output); // uses run method from test.hpp
}