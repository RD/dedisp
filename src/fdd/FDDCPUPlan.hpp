#ifndef DEDISP_FDD_CPU_PLAN_HPP_
#define DEDISP_FDD_CPU_PLAN_HPP_

#include "Plan.hpp"

#include <stddef.h>

namespace dedisp {

class FDDCPUPlan : public Plan {
  friend class FDDGPUPlan;

public:
  // Constructor
  FDDCPUPlan(size_type nchans, float_type dt, float_type f0, float_type df,
             int device_index = 0);

  // Destructor
  ~FDDCPUPlan();

  // Public interface
  virtual void execute(size_type nsamps, const byte_type *in,
                       size_type in_nbits, byte_type *out, size_type out_nbits,
                       unsigned flags = 0) override;

private:
  // Private interface
  virtual void execute_cpu(size_type nsamps, const byte_type *in,
                           size_type in_nbits, byte_type *out,
                           size_type out_nbits);

  virtual void execute_cpu_segmented(size_type nsamps, const byte_type *in,
                                     size_type in_nbits, byte_type *out,
                                     size_type out_nbits);

  // Helper methods
  void generate_spin_frequency_table(dedisp_size nfreq, dedisp_size nsamp,
                                     dedisp_float dt);

  void fft_r2c(unsigned int n, unsigned int batch, size_t in_stride,
               size_t out_stride, float *in, float *out);

  void fft_r2c_inplace(unsigned int n, unsigned int batch, size_t stride,
                       float *data);

  void fft_c2r(unsigned int n, unsigned int batch, size_t in_stride,
               size_t out_stride, float *in, float *out);

  void fft_c2r_inplace(unsigned int n, unsigned int batch, size_t stride,
                       float *data);

  // Host arrays
  std::vector<dedisp_float> h_spin_frequencies; // size = nfreq
};

} // end namespace dedisp

#endif // DEDISP_FDD_CPU_PLAN_HPP_