/*
 * Time Domain Dedispersion (TDD)
 * is an optimized version of the original dedisp implementation.
 */
#define TILE_DIM 32
#define BLOCK_ROWS 8

template <typename WordType>
__device__ void
transpose_kernel_dedisp_impl(const WordType *in, size_t width, size_t height,
                             size_t in_stride, size_t out_stride, WordType *out,
                             size_t block_count_x, size_t block_count_y) {
  __shared__ WordType tile[TILE_DIM][TILE_DIM];

  // Cull excess blocks (there may be many if we round up to a power of 2)
  if (blockIdx.x >= block_count_x || blockIdx.y >= block_count_y) {
    return;
  }

  size_t index_in_x = blockIdx.x * TILE_DIM + threadIdx.x;
  size_t index_in_y = blockIdx.y * TILE_DIM + threadIdx.y;
  size_t index_in = index_in_x + (index_in_y)*in_stride;

#pragma unroll
  for (size_t i = 0; i < TILE_DIM; i += BLOCK_ROWS) {
    // TODO: Is it possible to cull some excess threads early?
    if (index_in_x < width && index_in_y + i < height)
      tile[threadIdx.y + i][threadIdx.x] = in[index_in + i * in_stride];
  }

  __syncthreads();

  size_t index_out_x = blockIdx.y * TILE_DIM + threadIdx.x;
  // Avoid excess threads
  if (index_out_x >= height)
    return;
  size_t index_out_y = blockIdx.x * TILE_DIM + threadIdx.y;
  size_t index_out = index_out_x + (index_out_y)*out_stride;

#pragma unroll
  for (size_t i = 0; i < TILE_DIM; i += BLOCK_ROWS) {
    // Avoid excess threads
    if (index_out_y + i < width) {
      out[index_out + i * out_stride] = tile[threadIdx.x][threadIdx.y + i];
    }
  }
}

extern "C" {
#ifndef PARAM_DATA_TYPE
#define PARAM_DATA_TYPE unsigned int
#endif

__global__ void transpose_kernel_dedisp(const unsigned int *in, size_t width,
                                        size_t height, size_t in_stride,
                                        size_t out_stride, unsigned int *out,
                                        size_t block_count_x,
                                        size_t block_count_y) {
  transpose_kernel_dedisp_impl<PARAM_DATA_TYPE>(in, width, height, in_stride,
                                                out_stride, out, block_count_x,
                                                block_count_y);
}
} // end namespace tdd