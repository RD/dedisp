add_library(
  tdd SHARED
  cinterface.cpp
  TDDPlan.cpp
  dedisperse/TDDKernel.cu
  transpose/TransposeKernel.cu
  $<TARGET_OBJECTS:common>
  $<TARGET_OBJECTS:plan>
  $<TARGET_OBJECTS:external>)

target_include_directories(
  tdd
  PRIVATE "${CMAKE_SOURCE_DIR}/src"
  PRIVATE "${CMAKE_SOURCE_DIR}/src/tdd")

include(GNUInstallDirs)
list(APPEND CMAKE_INSTALL_RPATH ${CMAKE_INSTALL_FULL_LIBDIR})

target_compile_options(tdd PUBLIC $<$<COMPILE_LANGUAGE:CUDA>:-use_fast_math>)

target_embed_source(tdd transpose/tdd_transpose_kernel.cu)
target_embed_source(tdd dedisperse/tdd_dedisperse_kernel.cu)

target_link_libraries(
  tdd PUBLIC OpenMP::OpenMP_CXX cudawrappers::cu cudawrappers::nvtx
             cudawrappers::nvrtc Threads::Threads)

set_target_properties(
  tdd
  PROPERTIES VERSION ${DEDISP_VERSION}
             SOVERSION ${DEDISP_VERSION_MAJOR}
             PUBLIC_HEADER TDDPlan.hpp)

if(${DEDISP_BACKEND_HIP})
  get_target_property(sources tdd SOURCES)
  set_source_files_properties(${sources} PROPERTIES LANGUAGE HIP)
endif()

if(${DEDISP_BACKEND_HIP})
  set_source_files_properties(
    dedisperse/FDDKernel.cu transpose/TransposeKernel.cu PROPERTIES LANGUAGE
                                                                    HIP)
endif()

if(DEDISP_BENCHMARK_WITH_PMT)
  target_link_libraries(tdd PUBLIC pmt)
endif()

install(
  TARGETS tdd
  LIBRARY DESTINATION lib
  PUBLIC_HEADER DESTINATION include)
