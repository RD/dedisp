#include "GPUPlan.hpp"

#include <cassert>
#include <exception>
#include <iostream>

#include "cudawrappers/cu.hpp"

namespace dedisp {

// Public interface
GPUPlan::GPUPlan(size_type nchans, float_type dt, float_type f0, float_type df,
                 int device_idx)
    : Plan(nchans, dt, f0, df) {
  // Initialize CUDA
  cu::init();

  // Initialize device
  set_device(device_idx);

  // Initialize streams
  htodstream.reset(new cu::Stream());
  dtohstream.reset(new cu::Stream());
  executestream.reset(new cu::Stream());

  // Initialize delay table
  d_delay_table =
      std::make_unique<cu::DeviceMemory>(nchans * sizeof(dedisp_float));
  htodstream->memcpyHtoDAsync(*d_delay_table, h_delay_table.data(),
                              d_delay_table->size());

  // Initialize the killmask
  d_killmask = std::make_unique<cu::DeviceMemory>(nchans * sizeof(dedisp_bool));
  htodstream->memcpyHtoDAsync(*d_killmask, h_killmask.data(),
                              d_killmask->size());
}

// Destructor
GPUPlan::~GPUPlan() {}

void GPUPlan::set_device(int device_idx) {
  m_device.reset(new cu::Device(device_idx));
  m_context.reset(new cu::Context(CU_CTX_SCHED_BLOCKING_SYNC, *m_device));
}

void GPUPlan::generate_dm_list(float_type dm_start, float_type dm_end,
                               float_type ti, float_type tol) {
  Plan::generate_dm_list(dm_start, dm_end, ti, tol);

  d_dm_list =
      std::make_unique<cu::DeviceMemory>(m_dm_count * sizeof(dedisp_float));
  assert(d_dm_list);

  // Copy the DM list to the device
  htodstream->memcpyHtoDAsync(*d_dm_list, h_dm_list.data(), d_dm_list->size());
}

void GPUPlan::set_dm_list(const float_type *dm_list, size_type count) {
  Plan::set_dm_list(dm_list, count);

  d_dm_list =
      std::make_unique<cu::DeviceMemory>(m_dm_count * sizeof(dedisp_float));
  assert(d_dm_list);

  // Copy the DM list to the device
  htodstream->memcpyHtoDAsync(*d_dm_list, h_dm_list.data(), d_dm_list->size());
}

void GPUPlan::set_killmask(const bool_type *killmask) {
  Plan::set_killmask(killmask);

  // Copy the killmask to the device
  htodstream->memcpyHtoDAsync(*d_killmask, h_killmask.data(),
                              d_killmask->size());
}

} // end namespace dedisp
