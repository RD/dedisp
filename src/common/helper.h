#ifndef DEDISP_COMMON_HELPER_H_
#define DEDISP_COMMON_HELPER_H_

#include <cstddef>

namespace dedisp {

void memcpy2D(void *dstPtr, size_t dstWidth, const void *srcPtr,
              size_t srcWidth, size_t widthBytes, size_t height);

// Return total system (host) memory in Mbits
size_t get_total_memory();
// Return used system (host) memory in Mbits
size_t get_used_memory();
// Return free system (host) memory in Mbits
size_t get_free_memory();

} // end namespace dedisp

#endif // DEDISP_COMMON_HELPER_H_