#include "fdd/FDDGPUPlan.hpp"
#define CATCH_CONFIG_MAIN
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

#include "common/test.hpp"

#include "fdd/FDDGPUPlan.hpp"
#include "tdd/TDDPlan.hpp"

// Dedisp is not supported on HIP.
#ifndef __HIP__
#include "dedisp/DedispPlan.hpp"

TEST_CASE("Dedisp compare expected DM output") {
  const std::vector<float> expected_dms = {
      0.392292, 0.395637, 0.401067, 0.397621, 0.390937, 0.446373,
      0.401329, 0.405182, 0.391987, 0.391658, 0.391621};

  unsigned int seed = 0;
  TestInput test_input{&seed};
  TestOutput test_output;

  run<dedisp::DedispPlan>(test_input,
                          test_output); // uses run method from test.hpp

  REQUIRE(
      test_output.out_mean ==
      Catch::Approx(0.376468509).margin(std::numeric_limits<float>::epsilon()));
  REQUIRE(
      test_output.out_sigma ==
      Catch::Approx(0.002306607).margin(std::numeric_limits<float>::epsilon()));
  REQUIRE_THAT(test_output.calculated_dms,
               Catch::Matchers::Approx(expected_dms)
                   .margin(std::numeric_limits<float>::epsilon()));
}
#endif

TEST_CASE("tdd compare expected DM output") {
  const std::vector<float> expected_dms = {
      0.392292, 0.395637, 0.401067, 0.397621, 0.390937, 0.446373,
      0.401329, 0.405182, 0.391987, 0.391658, 0.391621};

  unsigned int seed = 0;
  TestInput test_input{&seed};
  TestOutput test_output;

  run<dedisp::TDDPlan>(test_input,
                       test_output); // uses run method from test.hpp

  REQUIRE(
      test_output.out_mean ==
      Catch::Approx(0.376468509).margin(std::numeric_limits<float>::epsilon()));
  REQUIRE(
      test_output.out_sigma ==
      Catch::Approx(0.002306607).margin(std::numeric_limits<float>::epsilon()));
  REQUIRE_THAT(test_output.calculated_dms,
               Catch::Matchers::Approx(expected_dms)
                   .margin(std::numeric_limits<float>::epsilon()));
}

TEST_CASE("fdd compare expected DM output") {
  const std::vector<float> expected_dms = {
      4.850260,  6.077153, 8.697535, 7.709451, 4.709877, 7.514279,
      20.041370, 6.095791, 7.270431, 9.196441, 4.596337, 4.517603};

  unsigned int seed = 0;
  TestInput test_input{&seed};
  TestOutput test_output;

  run<dedisp::FDDGPUPlan>(test_input,
                          test_output); // uses run method from test.hpp

  REQUIRE(test_output.out_mean ==
          Catch::Approx(-0.000709622)
              .margin(std::numeric_limits<float>::epsilon()));
  REQUIRE(test_output.out_sigma == Catch::Approx(0.747440).margin(
                                       std::numeric_limits<float>::epsilon()));
  REQUIRE_THAT(test_output.calculated_dms,
               Catch::Matchers::Approx(expected_dms)
                   .margin(std::numeric_limits<float>::epsilon()));
}
