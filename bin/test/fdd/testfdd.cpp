#include "fdd/FDDCPUPlan.hpp"
#include "fdd/FDDGPUPlan.hpp"

#include <iostream>

#include "../common/test.hpp"

template <typename PlanType> int run(const TestInput &, TestOutput &);

int main(int argc, char *argv[]) {
  TestInput test_input{};
  TestOutput test_output{};

  // Set environment variable USE_CPU to switch to CPU implementation of FDD
  char *use_cpu_str = getenv("USE_CPU");
  bool use_cpu = !use_cpu_str ? false : atoi(use_cpu_str);
  if (use_cpu) {
    std::cout << "Test FDD on CPU" << std::endl;
    return run<dedisp::FDDCPUPlan>(test_input, test_output);
  } else {
    std::cout << "Test FDD on GPU" << std::endl;
    return run<dedisp::FDDGPUPlan>(test_input, test_output);
  }
}