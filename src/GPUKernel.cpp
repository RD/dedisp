#include "GPUKernel.hpp"
#include <cudawrappers/cu.hpp>

#include <iostream>
#include <stdexcept>

void GPUKernel::assertCompiled(const CompiledKernelInfo &kernel_info) const {
  if (!kernel_info.function) {
    std::ostringstream ss;
    ss << func_name_ << " in file " << filename_
       << " has not yet been compiled";
    throw std::runtime_error(ss.str());
  }
}

std::pair<const CompiledKernelInfo &, bool>
GPUKernel::compile(const std::vector<std::string> &compile_options) {
  // Concat the compile options into a single string to be used as a map key.
  std::string options;

  if (compile_options.size()) {
    // Allocate some space beforehand to avoid repeated expanding allocations.
    options.reserve(compile_options.size() * 32);

    for (const auto &compile_option : compile_options) {
      options += compile_option;
    }
  }

  // Check if the function has already been compiled which the same options.
  if (compiled_kernels_.find(options) != compiled_kernels_.end()) {
    return {compiled_kernels_[options], false};
  }

  // Create a new map entry inplace.
  compiled_kernels_[options] = CompiledKernelInfo{};
  CompiledKernelInfo &info = compiled_kernels_[options];

  try {
    program_->compile(compile_options);
  } catch (nvrtc::Error &error) {
    std::cerr << program_->getLog();
    throw;
  }

  // Create a new module from the compiled PTX if needed.
  info.module = std::make_unique<cu::Module>(
      static_cast<const void *>(program_->getPTX().data()));

  if (!info.module) {
    throw std::runtime_error("Unable to create kernel module");
  }

  info.function = std::make_shared<cu::Function>(
      *info.module, std::string(func_name_).c_str());

  return {info, true};
}