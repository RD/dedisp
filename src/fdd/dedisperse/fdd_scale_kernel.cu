#ifndef FDD_SCALE_KERNEL_H_
#define FDD_SCALE_KERNEL_H_

extern "C" __global__ void scale_output_kernel(size_t n, size_t stride,
                                               float scale, float *d_data) {
  for (unsigned int i = threadIdx.x; i < n; i += blockDim.x) {
    d_data[blockIdx.x * stride + i] *= scale;
  }
}

#endif // FDD_SCALE_KERNEL_H_